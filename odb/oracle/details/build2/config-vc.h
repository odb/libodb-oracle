/* file      : odb/oracle/details/build2/config-vc.h
 * license   : ODB NCUEL; see accompanying LICENSE file
 */

/* Configuration file for Windows/VC++ for the build2 build. */

#ifndef ODB_ORACLE_DETAILS_CONFIG_VC_H
#define ODB_ORACLE_DETAILS_CONFIG_VC_H

/* Define LIBODB_ORACLE_BUILD2 for the installed case. */
#ifndef LIBODB_ORACLE_BUILD2
#  define LIBODB_ORACLE_BUILD2
#endif

#endif /* ODB_ORACLE_DETAILS_CONFIG_VC_H */

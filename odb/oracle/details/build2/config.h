/* file      : odb/oracle/details/build2/config.h
 * license   : ODB NCUEL; see accompanying LICENSE file
 */

/* Static configuration file for the build2 build. The installed case
   (when LIBODB_ORACLE_BUILD2 is not necessarily defined) is the only
   reason we have it. */

#ifndef ODB_ORACLE_DETAILS_CONFIG_H
#define ODB_ORACLE_DETAILS_CONFIG_H

/* Define LIBODB_ORACLE_BUILD2 for the installed case. */
#ifndef LIBODB_ORACLE_BUILD2
#  define LIBODB_ORACLE_BUILD2
#endif

#endif /* ODB_ORACLE_DETAILS_CONFIG_H */
